from django.db import models


# нужно ManyToManyField и ForiegnKey передать нормальное related_name(https://docs.djangoproject.com/en/1.10/topics/db/queries/#backwards-related-objects)
class journal(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)


class issue(models.Model):
    id = models.AutoField(primary_key=True)
    num = models.IntegerField()
    date = models.DateTimeField(auto_now=True)
    journal_id = models.ForeignKey(
        'journal',
        on_delete=models.CASCADE,
    )


class heading(models.Model):
    id = models.AutoField(primary_key=True)
    heading = models.CharField(max_length=128)
    issue_id = models.ForeignKey(
        'issue',
        on_delete=models.CASCADE
    )


class user(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    second_name = models.CharField(max_length=64)
    given_name = models.CharField(max_length=64)
    birthday = models.DateTimeField(auto_now=False)
    additional_info = models.TextField()
    login = models.CharField(max_length=64)
    password = models.CharField(max_length=256)
    security_lvl = models.CharField(max_length=2)
    is_activate = models.BooleanField()


class article(models.Model):
    id = models.AutoField(primary_key=True)
    heading_id = models.ForeignKey(
        'heading',
        on_delete=models.CASCADE
    )
    article_title = models.CharField(max_length=256)
    description = models.TextField()
    references = models.TextField()
    wac = models.CharField(max_length=128)
    thanks = models.TextField()
    comment = models.ManyToManyField(
        user,
        through='comments',
        through_fields=('article', 'user'),
    )
    authors = models.ManyToManyField(user, related_name='entries')


class comments(models.Model):
    article_id = models.ForeignKey(article, on_delete=models.CASCADE, related_name='com')
    user_id = models.ForeignKey(user, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    body = models.TextField()


class article_content(models.Model):
    id = models.AutoField(primary_key=True)
    article_id = models.ForeignKey(article, on_delete=models.CASCADE)
    content = models.FileField(
        upload_to='/upload')  # upload_to = 'upload/current_journal/current_issue/current_headings/article_name.txt'
    data = models.DateTimeField()
    feedback = models.ManyToManyField(
        user,
        through='feedbacks',
        through_fields=('article_content_id', 'user')
    )


class feedbacks(models.Model):
    user_id = models.ForeignKey(user, on_delete=models.CASCADE)
    article_content_id = models.ForeignKey(article_content, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    text = models.TextField()


class tag(models.Model):
    id = models.AutoField(primary_key=True)
    tag_body = models.CharField(max_length=64)
    tag_article = models.ManyToManyField(article, )
