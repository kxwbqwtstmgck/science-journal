"""scienceJournal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url

from . import views

app_name = 'main_page'

urlpatterns = (
    url(r'^$', views.index, name='index'),

    url(r'^about/$', views.about, name='about'),
    url(r'^add_issue/$', views.addIssue, name='add_issue'),
    url(r'^add_heading(?P<pk>[0-9]+)/$', views.addHeading, name='add_heading'),
    url(r'^article(?P<pk>[0-9]+)/$', views.article_view, name='article'),
    url(r'^article-premoderation/(?P<pk>[0-9]+)/$', views.article_premoderation_view,
        name='article-premoderation-view'),

    url(r'^delete_article/(?P<pk>[0-9]+)/$', views.delete_article, name='delete_article'),
    url(r'^download(?P<pk>[0-9]+)/$', views.download, name='download'),
    url(r'^send_comment$', views.send_comment, name='send_comment'),
    url(r'^delete_comment/(?P<pk>\d+)$', views.delete_comment, name='delete_comment'),
    url(r'^delete_user(?P<pk>\d+)$', views.delete_user, name='delete_user'),

    url(r'^list/$', views.list_article, name='list_page'),
    url(r'^list(?P<stat>[-\w.]{3})/$', views.list_article, name='list_page'),
    url(r'^list(?P<heading_pk>[0-9]+)/$', views.list_article, name='list_article'),
    url(r'^list_issue/$', views.list_issue, name='list_issue'),
    url(r'^list_heading(?P<issue_pk>[0-9]+)/$', views.list_heading, name='list_heading'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.LogOutView.as_view(), name='logout'),

    url(r'^main-page-edit/$', views.main_page_edit, name='main-page-edit'),

    url(r'^profile/$', views.update_profile, name='profile'),
    url(r'^profile/(?P<username>[-\w.]+)$', views.show_profile, name='show_profile'),
    url(r'^premoderation-article-list/$', views.premoderation_article_list, name='premoderation-article-list'),
    url(r'^pub_article/(?P<pk>[0-9]+)/$', views.pub_article, name='pub_article'),

    url(r'^search-author/$', views.search_author, name='search-author'),
    url(r'^search-author/(?P<author>[-\w.]+)$', views.list_by_author, name='list_by_author'),
    url(r'^send_article/$', views.send_article, name='send_article'),
    url(r'^send_content/(?P<article_pk>[0-9]+)/$', views.send_content, name='send_content'),
    url(r'^signup/$', views.RegisterFormView.as_view(), name='signup'),
    #password_reset urls
    url(r'^password_reset/$', views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', views.password_reset_complete, name='password_reset_complete'),


    url(r'^urls/$', views.all_urls, name='all_urls'),
)
