from django.test import TestCase
from .models import *
from .forms import *


class FormsTests(TestCase):
    def setUp(self):
        issue = Issue(num=0)
        issue.save()
        heading = Heading(heading='HEADING', issue_id=1)
        heading.save()

    def test_article_form_valid_correct(self):
        form = ArticleForm(data={'heading': 1, 'article_title': "Title", 'description': "Description", 'references': "asdfasdf", 'wac': 'WAC', 'thanks': "THANKS"})
        self.assertTrue(form.is_valid())

    def test_article_form_valid_incorrect(self):
        form = ArticleForm(data={'heading': 2, 'article_title': "Title", 'description': "Description", 'references': "asdfasdf", 'thanks': "THANKS"})
        self.assertFalse(form.is_valid())

    def test_user_form(self):
        form = UserForm(data={'first_name': 'User', 'last_name': 'LastName', 'email': 'mail@mail.com'})
        self.assertTrue(form.is_valid())


class ArticleListViewsTests(TestCase):
    def setUp(self):
        heading = Heading(heading='HEADING', issue_id=1)
        heading.save()
        for i in range(10):
            article = Article(article_title='TITLE_' + str(i) + '_PUB', status='PUB',
                              heading=heading, description='')
            article.save()

    def test_list_article(self):
        response = self.client.get('main/list/')
        self.assertNotEqual(response.status_code, 200)











