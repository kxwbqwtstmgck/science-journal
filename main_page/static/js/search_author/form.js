$(document).ready(function () {
    console.log('form.js');
    // $("#id_body").attr("placeholder", "Введите комментарий при отказе в публикации");
    $("#id_body").attr("required", "true");

    $("#id_search_button").on("click", function () {
        var authorSearch = $("#id_search_form").val();
        if (authorSearch) {
            window.location.href = authorSearch;
        }
    });
});