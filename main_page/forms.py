from django.contrib.auth.models import User
from bootstrap3_datetime.widgets import DateTimePicker
from django.forms import *
from .models import *
from ckeditor.fields import CKEditorWidget

class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ('cover', 'heading', 'article_title', 'description', 'references', 'wac', 'thanks')
        labels = {
            'cover': 'Обложка',
            'heading': 'Заголовок',
            'article_title': 'Название',
            'description': 'Описание',
            'references': 'Источники',
            'wac': 'WAC',
            'thanks': 'Благодарности',
        }


class ArticleContentForm(ModelForm):
    class Meta:
        model = ArticleContent
        fields = ('content',)
        labels = {
            'content': 'Файл статьи',
        }
        
    def cleaned_file(self):
        file = self.cleaned_data['content']
        error = None
        try:
            if file:
                file_type = file.content_type.split('/')[1]
            if len(file.name.split('.')) == 1:
                error = 'Данный формат не поддерживается'

            if file_type in settings.ARTICLE_CONTENT_TYPE:
                if file._size > settings.ARTICLE_MAX_SIZE:
                    error = 'Размер файла превышен'
            else:
                error = 'Данный формат не поддерживается'
        except:
            pass

        if error:
            return error
        else:
            return file


class IssueForm(ModelForm):
    class Meta:
        model = Issue
        fields = ('num', 'cover')


class HeadingForm(ModelForm):
    class Meta:
        model = Heading
        fields = ('heading',)


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')
        labels = {
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'email': 'Email',
        }


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ('body', 'article')


class ProfileForm(ModelForm):
    birth_date = DateField(input_formats=settings.DATE_INPUT_FORMATS,
                          widget=DateTimePicker(options={"format": "DD-MM-YYYY", "pickTime": False}))
    additionalInfo = CharField(widget=CKEditorWidget())

    class Meta:
        model = Profile
        fields = ('patronymic', 'avatar', 'birth_date', 'additionalInfo')
        labels = {
            'patronymic': 'Отчество',
            'additionalInfo': 'Дополнительная информация',
        }


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = ['body']
        labels = {
            'body': 'Введите причину отказа к публикации статьи',
        }


class MainPageInfoForm(ModelForm):
    class Meta:
        model = MainPageInfo
        fields = ['appeal', 'greetings', 'whatIsNew']
        labels = {
            'greetings': 'Приветствие',
            'appeal': 'Обращение разработчиков',
            'whatIsNew': 'Что нового',
        }