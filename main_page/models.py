from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
import datetime
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from ckeditor.fields import RichTextField

fs = FileSystemStorage(location='main_page/static')


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True)
    patronymic = models.CharField(max_length=30, blank=True)  # Отчество
    additionalInfo = RichTextField(default='')
    avatar = models.ImageField(upload_to='images', default='images/no_pic.png', storage=fs)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Issue(models.Model):
    num = models.IntegerField()
    date = models.DateTimeField(auto_now=True)
    cover = models.ImageField(upload_to='images', default='/images/no_pic.png', storage=fs)

    def __str__(self):
        return 'Tough life #' + str(self.num)


class Heading(models.Model):
    heading = models.CharField(max_length=128)
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)

    def __str__(self):
        return self.heading


class Article(models.Model):
    STATUS = (
        ('PUB', 'published'),
        ('PRM', 'pre-moderation'),
        ('MOD', 'moderated')
    )
    article_title = models.CharField(max_length=256)
    heading = models.ForeignKey(Heading, on_delete=models.CASCADE)
    description = models.TextField()
    references = models.TextField()
    wac = models.CharField(max_length=128)
    thanks = models.TextField()
    pub_date = models.DateTimeField('date published', default=timezone.now(), blank=True)
    status = models.CharField(max_length=3, choices=STATUS, default='PRM')
    cover = models.ImageField(upload_to='images', storage=fs)
    comment = models.ManyToManyField(
        User,
        through='Comment',
        through_fields=('article', 'user'),
        related_name='com'
    )
    authors = models.TextField()

    def __str__(self):
        return self.article_title + '\n' + self.description


class ArticleContent(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    content = models.FileField(upload_to='upload/articles/%Y/%m/%d')
    date = models.DateTimeField(auto_now=True)
    latest_version = models.BooleanField(default=True)
    feedback = models.ManyToManyField(
        User,
        through='Feedback',
        through_fields=('article_content_id', 'user')
    )


class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='com')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    body = models.TextField()


class Feedback(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    article_content = models.ForeignKey(ArticleContent, on_delete=models.CASCADE, related_name='fdbck')
    date = models.DateTimeField(auto_now=True)
    body = models.TextField()


# Инфа главной страницы
class MainPageInfo(models.Model):
    appeal = models.TextField(max_length=1024)
    greetings = models.TextField(max_length=1024)
    whatIsNew = models.TextField(max_length=1024)
