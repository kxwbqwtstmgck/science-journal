import os.path

from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm, SetPasswordForm
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from django.views.generic.edit import FormView
from libs.display_exceptions import NotFound
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.contrib.auth.tokens import default_token_generator
from django.views.decorators.csrf import csrf_protect
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters

from .forms import *  # для формы
from .models import Article, Issue, Heading, ArticleContent, Comment, MainPageInfo
from django.conf.urls import RegexURLPattern, RegexURLResolver
from django.core.exceptions import ObjectDoesNotExist


class LogOutView(generic.RedirectView):
    url = reverse_lazy('main_page:index')

    def get(self, request, *args, **kwargs):
        logout(self.request)
        return super(LogOutView, self).get(self.request, *args, **kwargs)


class RegisterFormView(FormView):
    form_class = UserCreationForm

    # Ссылка, на которую будет перенаправляться пользователь в случае успешной регистрации.
    # В данном случае указана ссылка на страницу входа для зарегистрированных пользователей.
    success_url = reverse_lazy('main_page:profile')

    # Шаблон, который будет использоваться при отображении представления.
    template_name = "form.html"

    def form_valid(self, form):
        # Создаём пользователя, если данные в форму были введены корректно.
        form.save()
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
        if user is not None:
            login(self.request, user)
        # Вызываем метод базового класса
        return super(RegisterFormView, self).form_valid(form)


def about(request):
    context = {'page_name': 'about'}
    return render(request, './about.html', context)


def addIssue(request):
    if request.method == 'POST':
        form = IssueForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('main_page:list_issue')
    else:
        form = IssueForm()
    return render(request, './form.html', {'form': form, 'page_name': 'add_issue'})


def addHeading(request, pk):
    if request.method == 'POST':
        form = HeadingForm(request.POST)
        if form.is_valid():
            heading = form.save(commit=False)
            heading.issue = Issue.objects.get(pk=pk)
            heading.save()
            return redirect('main_page:list_heading', issue_pk=pk)
    else:
        form = HeadingForm()
    return render(request, './form.html', {'form': form, 'page_name': 'add_heading'})


def all_urls(request):
    from .urls import urlpatterns  # this import should be inside the function to avoid an import loop
    nice_urls = get_urls(urlpatterns)  # build the list of urls recursively and then sort it alphabetically
    return render(request, "all_urls.html", {"links": nice_urls})


def delete_user(request, pk):
    User.objects.get(pk=pk).delete()
    return redirect('main_page:index')


def main_page_redirect(request):
    return redirect('/main/')


def show_profile(request, username):
    try:
        user = User.objects.get(username=username)
        profile = user.profile
        return render(request, 'show_profile.html', {'profile': profile})
    except ObjectDoesNotExist:
        raise NotFound('User not found!.', next=resolve_url(request.META.get('HTTP_REFERER', 'main_page:index')))


def article_premoderation_view(request, pk):
    article_content = ArticleContent.objects.filter(article=Article.objects.get(pk=pk)).get(latest_version=True)
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.user = User.objects.get(pk=request.user.id)
            new_comment.article_content = article_content
            new_comment.save()
            article = article_content.article
            article.status = 'MOD'
            article.save()
        return redirect('main_page:index')
    else:
        form = FeedbackForm()
    return render(request, 'article.html',
                  {'article': article_content.article, 'page_name': 'article-premoderation', 'premoderation': 1,
                   'form': form})


def article_view(request, pk):
    article = Article.objects.get(pk=pk)
    return render(request, 'article.html', {'article': article, 'page_name': 'article', 'user': request.user,
                                            'comments': Comment.objects.filter(article=pk)})


def delete_article(request, pk):
    Article.objects.get(pk=pk).delete()
    return redirect('main_page:list_page')


def delete_comment(request, pk):
    Comment.objects.get(pk=pk).delete()
    return redirect(request.META.get('HTTP_REFERER', 'main_page:index'))


def download(request, pk):
    art = ArticleContent.objects.get(article=Article.objects.get(pk=pk), latest_version=True)
    file_name = os.path.basename(art.content.path)
    response = HttpResponse(content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=' + file_name
    response['X-Sendfile'] = file_name
    return response


def get_urls(raw_urls, nice_urls=[], urlbase=''):
    '''Recursively builds a list of all the urls in the current project and the name of their associated view'''
    from operator import itemgetter
    for entry in raw_urls:
        fullurl = entry.regex.pattern.replace('^', '')
        viewname = entry.name
        # split_fullurl = entry.regex.pattern.split('/', fullurl)
        # split_fullurl = fullurl[:fullurl.find('/', '(')]
        split_fullurl = fullurl.split('/')
        split_fullurl = split_fullurl[0].split('$')
        split_fullurl = split_fullurl[0].split('(')
        # print(fullurl)
        nice_urls.append({"name": viewname, "pattern": split_fullurl[0]})
    nice_urls = sorted(nice_urls, key=itemgetter('pattern'))  # sort alphabetically
    return nice_urls


def index(request):
    MainPage = {'header': "Tough Life",
                'headerDescription': "Официальная страница научного журнала",
                'aboutHeader': "О журнале",
                'aboutText': "Журнал является прототипом научного журнала в вакууме. Никаких научных статей. Разработчики: Бутолин Дмитрий, Гильмитдинов Ноэль, Шавлюк Михаил и Леха.",
                'archivesHeader': "Архив",
                'elsewhereHeader': "Мы в социальных сетях"}
    MainPageDBInfo = MainPageInfo.objects.last()
    context = {'MainPage': MainPage, 'page_name': 'index', 'MainPageDBInfo': MainPageDBInfo}
    return render(request, 'index.html', context)


def list_article(request, heading_pk=None, stat='pub'):
    if heading_pk == None:
        articles = Article.objects.filter(status=stat.upper())
    else:
        articles = Article.objects.filter(heading=Heading.objects.get(pk=heading_pk)).filter(status=stat.upper())
    context = {'page_name': 'list_page', 'articles': articles}
    return render(request, 'list_page.html', context)


def list_by_author(request, author):
    try:
        author = User.objects.get(username=author)
        articles = Article.objects.filter(authors=author)
    except:
        raise NotFound('Error.', next=resolve_url('main_page:search-author'))
    context = {'page_name': 'list_page', 'articles': articles}
    return render(request, 'list_page.html', context)


def list_heading(request, issue_pk):
    headings = Heading.objects.filter(issue=Issue.objects.get(pk=issue_pk))
    context = {'page_name': 'list_heading', 'headings': headings, 'issue': Issue.objects.get(pk=issue_pk)}
    return render(request, './list_heading.html', context)


def list_issue(request):
    issues = Issue.objects.all()
    context = {'page_name': 'list_issue', 'issues': issues}
    return render(request, './list_issue.html', context)


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse_lazy('main_page:index'))
        else:
            raise NotFound('A username is needed to look up a user.', next=resolve_url('main_page:index'))
    else:  # Отображаем страницу входа
        ...


def pub_article(request, pk):
    article = Article.objects.get(pk=pk)
    article.status = 'PUB'
    article.save()
    return redirect('main_page:list_page')


def premoderation_article_list(request):
    articles = Article.objects.filter(status='PRM')
    context = {'page_name': 'premoderation-article-list', 'articles': articles}
    return render(request, './premoderation_article_list.html', context)


def page_not_found_view(request):
    raise NotFound('Page not found!.', next=resolve_url('main_page:index'))


def send_article(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            article = form.save(commit=False)
            article.authors = User.objects.get(pk=request.user.id).username
            article.save()
            return HttpResponseRedirect(reverse_lazy('main_page:send_content', kwargs={'article_pk': article.pk}))
    else:
        form = ArticleForm()
    return render(request, './send_article.html', {'form': form, 'page_name': 'send_article'})


def send_content(request, article_pk):
    if request.method == 'POST':
        form = ArticleContentForm(request.POST, request.FILES)
        if form.is_valid():
            article = Article.objects.get(pk=article_pk)
            old_content = ArticleContent.objects.filter(article=article)
            if old_content:
                for content in old_content:
                    if content.latest_version:
                        content.latest_version = False
                        content.save()
            if type(form.cleaned_file()) != str:
                new_content = ArticleContent(content=form.cleaned_file(), article=article)
                new_content.save()
                article.status = 'PRM'
                article.save()
                return redirect('main_page:index')
            else:
                return redirect(reverse_lazy('main_page:send_content', kwargs={'article_pk': article_pk}))
    else:
        form = ArticleContentForm()
    return render(request, './send_article.html', {'form': form, 'page_name': 'send_content'})


def send_comment(request):
    if request.method == 'POST':
        comment = CommentForm(request.POST)
        if comment.is_valid():
            comment = comment.save(commit=False)
            comment.user = request.user
            comment.save()
        return redirect(request.META.get('HTTP_REFERER', 'main_page:index'))


def search_author(request):
    context = {'page_name': 'search-author'}
    return render(request, './search_author.html', context)


def update_profile(request):
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if profile_form.is_valid():
            profile_form.save()

            return redirect('main_page:show_profile', request.user.username)
            # messages.success(request, _('Your profile was successfully updated!'))
            # else:
            # messages.error(request, _('Please correct the error below.'))
    else:
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'form.html', {
        'form': profile_form
    })


@csrf_protect
def password_reset(request,
                   template_name='password_reset_form.html',
                   email_template_name='password_reset_email.html',
                   subject_template_name='password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   extra_context=None,
                   html_email_template_name=None,
                   extra_email_context=None):
    if post_reset_redirect is None:
        post_reset_redirect = reverse_lazy('main_page:password_reset_done')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
                'extra_email_context': extra_email_context,
            }
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)
    else:
        form = password_reset_form()
    context = {
        'form': form,
        'title': 'Password reset',
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def password_reset_done(request,
                        template_name='password_reset_done.html',
                        extra_context=None):
    context = {
        'title': 'Password reset sent',
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def main_page_edit(request):
    if request.method == 'POST':
        form = MainPageInfoForm(request.POST)
        if form.is_valid():
            mainPageInfo1 = form.save(commit=False)
            mainPageInfo1.save()
            return redirect('main_page:index')
    else:
        form = MainPageInfoForm()
    return render(request, './main-page-edit.html', {'form': form, 'MainPageInfo': MainPageInfo})


@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm,
                           post_reset_redirect=None,
                           extra_context=None):
    UserModel = get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse_lazy('main_page:password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = 'Enter new password'
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = 'Password reset unsuccessful'
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


def password_reset_complete(request,
                            template_name='password_reset_complete.html',
                            extra_context=None):
    context = {
        'login_url': resolve_url(settings.LOGIN_URL),
        'title': 'Password reset complete',
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)
